from setuptools import setup, find_packages

setup(name='bmwsensors',
      version='v0.1',
      description='Optimization of Sensor Placement for the BMW quantum '
                  'computing challene',
      author='Tyler Wilson',
      author_email='tyler.l.wilson@gmail.com',
      packages=find_packages(),
      install_requires=['numpy', 'pandas','sympy','dwave-ocean-sdk',
                        'pytest', 'black', 'pylint', 'flake8', 'tqdm'],
      entry_points={'console_scripts': ['sensors = bmwsensors:main']}
      )