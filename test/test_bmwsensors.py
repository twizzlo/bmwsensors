import pytest
import unittest
import sys
import numpy as np
from copy import copy

sys.path.insert(0, "/Users/twilson/Research/BMWSensors/bmwsensors")
import bmwsensors as bmw

CI_INDEX = 99.0


@pytest.fixture
def make_sens_config(num_sens=1):
    coords = [-5, 5, -5, 5, -5, -5, 5, 5, 0, 0, 0, 0]
    sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
    sens_loc = np.array([0, 0, 0])
    theta_max = np.pi / 2
    phi_max = np.pi / 2
    sens_orient = np.array([np.pi / 2, 0])  ## aligned with y and z axis
    rng = 10
    sens_config = {
        "type": "Lidar",
        "theta_max": theta_max,
        "phi_max": phi_max,
        "sens_range": rng,
        "sensor_region": sr,
        "location": sens_loc,
        "orientation": sens_orient,
    }
    sens = bmw.Sensor(**sens_config)
    sensorconfig = bmw.SensorConfiguration([sens])
    return sensorconfig


@pytest.fixture
def make_2sens_config(num_sens=1):
    coords = [-5, 5, -5, 5, -5, -5, 5, 5, 0, 0, 0, 0]
    sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
    sens_loc = np.array([0, 0, 0])
    theta_max = np.pi / 2
    phi_max = np.pi / 2
    sens_orient = np.array([np.pi / 2, 0])  ## aligned with y and z axis
    rng = 10
    sens_config = {
        "type": "Lidar",
        "theta_max": theta_max,
        "phi_max": phi_max,
        "sens_range": rng,
        "sensor_region": sr,
        "location": sens_loc,
        "orientation": sens_orient,
    }
    sens = bmw.Sensor(**sens_config)
    sens2 = bmw.Sensor(**sens_config)
    sens2.location = np.array([1, 0, 0])
    sensorconfig = bmw.SensorConfiguration([sens, sens2])
    return sensorconfig


@pytest.fixture
def make_cubic_roi():
    return bmw.CubicROI(
        np.array([-1, 1]),
        np.array([1, 2]),
        critical_index=CI_INDEX,
        z_range=np.array([3, 5]),
    )


@pytest.fixture
def make_sector_roi():
    return bmw.SectorROI(
        centre=np.array([0, 0, 1]),
        min_radius=0,
        max_radius=1,
        centre_angle=np.pi / 2,
        angle_of_view=np.pi / 4,
        critical_index=CI_INDEX,
        height=1,
    )


class TestSensorRegion(unittest.TestCase):
    def test_check_validity(self):
        pass

    def test_xplane_region(self):
        coords = [1, 1, 1, 1, 0.1, 0.2, 0.3, 0.4, 1.1, 1.2, 1.3, 1.4]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        should_be_zhat = np.array([1, 0, 0])
        should_be_xhat = np.array([0, 0.1, 0.1]) / np.sqrt(0.1**2 + 0.1**2)
        should_be_yhat = np.array([0, -0.1, 0.1]) / np.sqrt(0.1**2 + 0.1**2)
        np.testing.assert_array_almost_equal(sr.zhat, should_be_zhat)
        np.testing.assert_array_almost_equal(sr.xhat, should_be_xhat)
        np.testing.assert_array_almost_equal(sr.yhat, should_be_yhat)

    def test_yplane_region(self):
        coords = [
            1.1,
            1.2,
            1.3,
            1.4,
            1,
            1,
            1,
            1,
            0.1,
            0.2,
            0.3,
            0.4,
        ]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        should_be_zhat = np.array([0, 1, 0])
        should_be_xhat = np.array([0.1, 0, 0.1]) / np.sqrt(0.1**2 + 0.1**2)
        should_be_yhat = np.array([0.1, 0, -0.1]) / np.sqrt(0.1**2 + 0.1**2)
        np.testing.assert_array_almost_equal(sr.zhat, should_be_zhat)
        np.testing.assert_array_almost_equal(sr.xhat, should_be_xhat)
        np.testing.assert_array_almost_equal(sr.yhat, should_be_yhat)

    def test_zplane_region(self):
        coords = [0.1, 0.2, 0.3, 0.4, 1.1, 1.2, 1.3, 1.4, 1, 1, 1, 1]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        should_be_zhat = np.array([0, 0, 1])
        should_be_xhat = np.array([0.1, 0.1, 0]) / np.sqrt(0.1**2 + 0.1**2)
        should_be_yhat = np.array([-0.1, 0.1, 0]) / np.sqrt(0.1**2 + 0.1**2)
        np.testing.assert_array_almost_equal(sr.zhat, should_be_zhat)
        np.testing.assert_array_almost_equal(sr.xhat, should_be_xhat)
        np.testing.assert_array_almost_equal(sr.yhat, should_be_yhat)

    def test_general_plane_region(self):
        coords = [1, 2, 1, 6, 1, 2, 2, 11, 3, 3, 4, 8]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        should_be_zhat = np.array([1, -1, 1]) / np.sqrt(3)
        should_be_xhat = np.array([1, 1, 0]) / np.sqrt(2)
        should_be_yhat = np.array([-1, 1, 2]) / np.sqrt(6)
        np.testing.assert_array_almost_equal(sr.zhat, should_be_zhat)
        np.testing.assert_array_almost_equal(sr.xhat, should_be_xhat)
        np.testing.assert_array_almost_equal(sr.yhat, should_be_yhat)

    def test_get_length(self):
        coords = [2, 1, 5, 4, 1, 0, 1, 0, 2, 3, 1, 2]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        pt = np.array([0, 0, 0])
        should_be = 3.0
        assert sr.get_length(pt) == should_be

        should_be = np.sqrt(11)
        actual = sr.get_length(pt, np.array([-1, 1, 3]))
        np.testing.assert_almost_equal(actual, should_be)

    def test_get_theta(self):
        coords = [2, 1, 5, 4, 1, 0, 1, 0, 2, 3, 1, 2]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        point = np.array([1, 7, 3])
        ref_point = np.array([2, 1, 2])

    def test_get_phi(self):
        coords = [2, 1, 5, 4, 1, 0, 1, 0, 2, 3, 1, 2]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        point = np.array([1, 7, 3])
        ref_point = np.array([2, 1, 2])

        should_be = np.arccos(14 / np.sqrt(38) / np.sqrt(14))
        actual = sr.get_phi(point)
        np.testing.assert_almost_equal(actual, should_be)

    def test_get_theta(self):
        coords = [2, 1, 5, 4, 1, 0, 1, 0, 2, 3, 1, 2]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        point = np.array([1, 7, 3])
        ref_point = np.array([2, 1, 2])
        n = np.array([1, 2, 3])
        pnt_on_plane = point - (np.dot(n, point) - 10) * n / np.dot(n, n)
        pnt_from_new_origin = pnt_on_plane - np.array([2, 1, 2])
        xhat = np.array([-1, -1, 1]) / np.sqrt(3)
        x = np.dot(pnt_from_new_origin, xhat)
        # TODO check why pi needs to be here
        should_be = -np.arccos(x / np.linalg.norm(pnt_from_new_origin))
        actual = sr.get_theta(point)
        np.testing.assert_almost_equal(actual, should_be)


class TestSensor(unittest.TestCase):
    def test_can_see_fixed(self):
        coords = [2, 1, 5, 4, 1, 0, 1, 0, 2, 3, 1, 2]
        sr = bmw.SensorRegion("Region1", *coords, ["Lidar"])
        xhat = sr.xhat
        yhat = sr.yhat
        zhat = sr.zhat
        sens_loc = np.array([0, 5, 0])
        sens_orient = np.array([np.pi / 4, np.pi / 4])
        theta_maxes = [np.pi / 6, np.pi / 3, np.pi / 2]
        phi_maxes = [np.pi / 6, np.pi / 3, np.pi / 2]
        rngs = [0.5, 1, 2]
        theta_tps = [0.0, np.pi / 3]
        phi_tps = [0.0, np.pi / 3]
        r_tps = [1.0]
        for r_tp in r_tps:
            for theta_tp in theta_tps:
                for phi_tp in phi_tps:
                    if phi_tp == 0.0:
                        theta_tp = 0.0
                    test_point_pos = (
                        sens_loc
                        + r_tp
                        * np.sin(phi_tp)
                        * (np.sin(theta_tp) * yhat + np.cos(theta_tp) * xhat)
                        + r_tp * np.cos(phi_tp) * zhat
                    )
                    test_point = bmw.RoiPoint(
                        *test_point_pos, critical_index=1.0
                    )
                    for theta_max in theta_maxes:
                        for phi_max in phi_maxes:
                            for rng in rngs:
                                sens_config = {
                                    "type": "Lidar",
                                    "theta_max": theta_max,
                                    "phi_max": phi_max,
                                    "sens_range": rng,
                                    "sensor_region": sr,
                                    "location": sens_loc,
                                    "orientation": sens_orient,
                                }
                                sens = bmw.Sensor(**sens_config)

                                in_range = rng >= 1
                                in_theta = theta_max >= 2 * abs(
                                    sens_orient[0] - theta_tp
                                )
                                in_phi = phi_max >= 2 * abs(
                                    sens_orient[1] - phi_tp
                                )
                                should_be = float(in_range & in_theta & in_phi)
                                actual = sens.can_see(test_point)
                                np.testing.assert_equal(actual, should_be)


class TestSensorConfiguration:
    CI_INDEX = 99.0

    def test_full_coverage_cubic_roi(self, make_sens_config, make_cubic_roi):
        reachable_roi = make_cubic_roi
        sensorconfig = make_sens_config
        points = reachable_roi.sample_points(1000)
        should_be = CI_INDEX
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_too_far_cubic_roi(self, make_sens_config, make_cubic_roi):
        too_far_roi = make_cubic_roi
        too_far_roi.z_range = np.array([10, 20])
        sensorconfig = make_sens_config
        points = too_far_roi.sample_points(1000)
        should_be = 0.0
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_outside_theta_cubic_roi(self, make_sens_config, make_cubic_roi):
        outside_theta_roi = make_cubic_roi
        outside_theta_roi.x_range = np.array([2, 3])
        sensorconfig = make_sens_config
        points = outside_theta_roi.sample_points(1000)
        should_be = 0.0
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_outside_phi_cubic_roi(self, make_sens_config, make_cubic_roi):
        outside_theta_roi = make_cubic_roi
        outside_theta_roi.z_range = np.array([0, 0.9])
        sensorconfig = make_sens_config
        points = outside_theta_roi.sample_points(1000)
        should_be = 0.0
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_full_coverage_sector_roi(self, make_sens_config, make_sector_roi):
        reachable_roi = make_sector_roi
        sensorconfig = make_sens_config
        points = reachable_roi.sample_points(1000)
        should_be = CI_INDEX
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_too_far_sector_roi(self, make_sens_config, make_sector_roi):
        too_far_roi = make_sector_roi
        too_far_roi.min_radius = 10
        too_far_roi.max_radius = 15
        sensorconfig = make_sens_config
        points = too_far_roi.sample_points(1000)
        should_be = 0.0
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_outside_theta_sector_roi(self, make_sens_config, make_sector_roi):
        outside_theta_roi = make_sector_roi
        outside_theta_roi.centre_angle = 0.0
        sensorconfig = make_sens_config
        points = outside_theta_roi.sample_points(1000)
        should_be = 0.0
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_outside_phi_sector_roi(self, make_sens_config, make_sector_roi):
        outside_phi_roi = make_sector_roi
        outside_phi_roi.centre = np.array([0.0, 0.0, 0.0])
        outside_phi_roi.min_radius = 2
        outside_phi_roi.max_radius = 3
        outside_phi_roi.height = 1
        sensorconfig = make_sens_config
        points = outside_phi_roi.sample_points(1000)
        should_be = 0.0
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_change_loc_cubic_roi(self, make_sens_config, make_cubic_roi):
        outside_theta_roi = make_cubic_roi
        outside_theta_roi.x_range = np.array([2, 4])
        deltaloc_orient = np.array([3, 0, 0, 0])
        sensorconfig = make_sens_config
        points = outside_theta_roi.sample_points(1000)
        should_be = CI_INDEX
        actual, _ = sensorconfig.coverage(
            points, only_active=False, delta_loc_orient=deltaloc_orient
        )

        np.testing.assert_array_equal(should_be, actual)

    def test_change_theta_sector_roi(self, make_sens_config, make_sector_roi):
        outside_theta_roi = make_sector_roi
        outside_theta_roi.centre_angle = 0.0
        deltaloc_orient = np.array([0, 0, -np.pi / 2, 0.0])
        sensorconfig = make_sens_config
        points = outside_theta_roi.sample_points(1000)
        should_be = CI_INDEX
        actual, _ = sensorconfig.coverage(
            points, only_active=False, delta_loc_orient=deltaloc_orient
        )
        np.testing.assert_array_equal(should_be, actual)

    def test_change_phi_sector_roi(self, make_sens_config, make_sector_roi):
        outside_phi_roi = make_sector_roi
        outside_phi_roi.x_range = np.array([2, 4])
        deltaloc_orient = np.array([0, 0, 0, np.pi / 4])
        sensorconfig = make_sens_config
        points = outside_phi_roi.sample_points(1000)
        should_be = CI_INDEX
        actual, _ = sensorconfig.coverage(
            points, only_active=False, delta_loc_orient=deltaloc_orient
        )
        np.testing.assert_array_equal(should_be, actual)

    def test_outside_two_sensors(self, make_2sens_config, make_cubic_roi):
        outside_cubic_roi = make_cubic_roi
        outside_cubic_roi.x_range = np.array([3, 5])
        sensorconfig = make_2sens_config
        points = outside_cubic_roi.sample_points(1000)
        should_be = np.zeros([2, 1000])
        actual, _ = sensorconfig.coverage(points, only_active=False)
        np.testing.assert_array_equal(should_be, actual)

    def test_change_two_sensors(self, make_2sens_config, make_cubic_roi):
        outside_cubic_roi = make_cubic_roi
        outside_cubic_roi.x_range = np.array([2, 4])
        sensorconfig = make_2sens_config
        points = outside_cubic_roi.sample_points(1000)
        deltaloc_orient = np.array([2, 0, 0.0, 0.0, 3, 0, 0, 0.0, 0.0])
        should_be = CI_INDEX
        cov, _ = sensorconfig.coverage(
            points, only_active=False, delta_loc_orient=deltaloc_orient
        )
        actual = cov.max(axis=0)
        np.testing.assert_array_equal(should_be, actual)
