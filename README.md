# bmwsensors

## Description
This is a well loved, but unfortunately abandoned project for the BMW quantum challenge project to optimize self-driving car sensor placement. It does not fully function. What is currently represented here is a cleaned up version of the code that was abandoned in 2021.

## Installation
Requires python 3.9. Previous versions of python 3 may be possible, but they are untested.

1. Create virtual environment
2. Enter virtual environment
2. Clone repo
2. ```pip install -e . ```

## Usage
Feel free to run ```sensors --help``` which will bring up the imagined command line interface options. However, no other functionality has been tested. As mentioned, this project was abandoned 'as-is', as has merely been updated to bring the project up to my current coding standards. It has NOT been updated to get it fully functional, running, and returning results. Though, to be honest,  I don't think it's too far off. It would be fun to finish it at some point.

## Project status
Abandoned, but loved, and dearly missed.
