from __future__ import annotations
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Callable
from copy import copy
import numpy as np
import pandas as pd
from tabu import TabuSampler
import dimod
from scipy.optimize import minimize
from tqdm import tqdm
import click

# TODO: Dear future me. The development of this code stalled at the 'optimize
#  the sensor positions/orientiations' phase. The idea is to use a gradient
#  free method to evaluate coverage at points (4 x num sensors dimensions)
#  such as Bayesian Optimization. The main issue are determining the
#  constraints on possible 'next' guesses. An example to problem would be to
#  perform an optimization on a non-regular quadrilateral. Current idea would
#  be to put a severe cost on new locations that are outside the sensor
#  regions. This in turn requires knowing if a proposed location is IN the
#  sensor region (which is the problem of determining if a point is in a
#  non-regular quadrilateral). This is the point that the work stalled. For
#  a refresher, the overall strategy is 1. #  Use ROIs to generate points,
#  2. Use Sensors to determine if they can see those points, 3. loop over all
#  points and see which sensors cover which points, 4. use the coverage and
#  intersections to make a qubo and optimize the sensor types, 5. once sensor
#  types are optimized, sensor locations are optimized using the gradient
#  free optimization methods. 6. Go back to step 4 and repeat. Alternatively,
#  another approach would be to optimize sensor types and locations
#  simultaneosuly using Xanadu's photon states (measure number of photons in
#  one channel for sensor type, measure (cnts) x projection in another channel
#  for location and cnts x projection in yet another channel for orientation.

SAME_LOCATION_PENALTY = 100000


@dataclass
class RoiPoint:
    """Class to specify a single point in a Region of Interest"""

    x_coord: float
    y_coord: float
    z_coord: float
    critical_index: float

    @property
    def pos(self) -> np.ndarray:
        """Return position of point"""
        return np.array([self.x_coord, self.y_coord, self.z_coord])


@dataclass
class PointSampler:
    """Class to generate test points for regions of interest"""

    rois: list

    def generate_points(self, num_points: int) -> list[RoiPoint]:
        """Generate random points in the regions of interest

        The points generated are weighted by the importance of the Region of
        Interest. That is, if a region has a higher value (critical index) more
        points will be selected from it

        Args:
            num_points: number of points to generate

        Returns:
            list: list of ROI points
        """
        sample_weights = np.array([roi.weighted_volume for roi in self.rois])
        sample_ratios = sample_weights / sum(sample_weights)
        region_num_samples = np.round(sample_ratios * num_points).astype(int)
        points = []
        region_total = 0
        for i, roi in enumerate(self.rois):
            region_total += roi.critical_index * region_num_samples[i]
            points.extend(roi.sample_points(region_num_samples[i]))
        return points


@dataclass
class SensorRegion:
    # pylint:disable=too-many-instance-attributes
    """Class representing a cubic shaped Region of Interest"""

    region: str
    x_coord1: float
    x_coord2: float
    x_coord3: float
    x_coord4: float
    y_coord1: float
    y_coord2: float
    y_coord3: float
    y_coord4: float
    z_coord1: float
    z_coord2: float
    z_coord3: float
    z_coord4: float
    allowed_types: list

    def __post_init__(self):
        self.pnt1 = np.array([self.x_coord1, self.y_coord1, self.z_coord1])
        self.pnt2 = np.array([self.x_coord2, self.y_coord2, self.z_coord2])
        self.pnt3 = np.array([self.x_coord3, self.y_coord3, self.z_coord3])
        self.pnt4 = np.array([self.x_coord4, self.y_coord4, self.z_coord4])
        z_vector = np.cross(self.pnt2 - self.pnt1, self.pnt3 - self.pnt1)
        self.zhat = z_vector / np.linalg.norm(z_vector)
        if ~self.is_valid():
            raise ValueError(f"Region {self.region} is invalid")
        x_vector = self.pnt2 - self.pnt1
        self.xhat = x_vector / np.linalg.norm(x_vector)
        y_vector = np.cross(z_vector, x_vector)
        self.yhat = y_vector / np.linalg.norm(y_vector)
        self.matrix_inverse = self._make_matrix_inv()

    def is_valid(self) -> bool:
        """Check to ensure region defines a valid plane"""
        # TODO: add colinearity check
        vector_1 = np.dot(self.pnt2 - self.pnt1, self.zhat)
        vector_2 = np.dot(self.pnt3 - self.pnt1, self.zhat)
        vector_3 = np.dot(self.pnt4 - self.pnt1, self.zhat)
        return (vector_1 < 1e-12) & (vector_2 < 1e-12) & (vector_3 < 1e-12)

    def get_theta(self, point: RoiPoint, ref_pt: RoiPoint = None) -> float:
        """Get elevation angle of passed point from sensor region's plane

        Args:
            point: point to obtain elevation angle for
            ref_pt: alternative origin, origin is sensor region's p1

        Returns:
            float: point's elevation angle from sensor region
        """
        if ref_pt is None:
            ref_pt = self.pnt1
        delta = self.get_local_pos(point) - self.get_local_pos(ref_pt)
        delta_y, delta_x = delta[1], delta[0]
        return np.arctan2(delta_y, delta_x)

    def get_phi(self, point: RoiPoint, ref_pt: RoiPoint = None) -> float:
        """Get azimuth angle of passed point from sensor region's plane

        Args:
            point: point to obtain azimuth angle for
            ref_pt: alternative origin, origin is sensor region's p1

        Returns:
            float: point's azimuth angle from sensor region
        """
        if ref_pt is None:
            ref_pt = self.pnt1
        delta = self.get_local_pos(point) - self.get_local_pos(ref_pt)
        delta_z = delta[2]  # > 0 (otherwise below plane)
        hypot = np.linalg.norm(delta)
        return np.arccos(delta_z / hypot)

    def get_length(self, point: RoiPoint, ref_pt: RoiPoint = None) -> float:
        """Get distance of passed point from ref pt

        Args:
            point: point to obtain distance for
            ref_pt: alternative origin, origin is sensor region's p1

        Returns:
            float: point's distance from reference point
        """
        if ref_pt is None:
            ref_pt = self.p1
        delta = self.get_local_pos(point) - self.get_local_pos(ref_pt)
        return np.linalg.norm(delta)

    def get_local_pos(self, point: RoiPoint, ref_pt: RoiPoint = None) -> np.ndarray:
        """Get position of passed point relative to ref pt

        Args:
            point: point to obtain position for
            ref_pt: alternative origin, origin is sensor region's p1

        Returns:
            float: point's position relative to ref pt
        """
        if ref_pt is None:
            ref_pt = self.pnt1
        point = point - ref_pt
        return np.matmul(self.A_inverse, point)

    def _make_matrix_inv(self):
        """Coordinate change matrix"""
        matrix = np.array([self.xhat, self.yhat, self.zhat]).transpose()
        return np.linalg.inv(matrix)

    @classmethod
    def from_dict(cls, spec_dict: dict) -> SensorRegion:
        """Create SensorRegion from spec dictionary"""
        spec_dict = spec_dict.copy()
        spec_dict["region"] = spec_dict.pop("Region")
        spec_dict["allowed_types"] = spec_dict.pop("Allowed Sensors")
        return cls(**spec_dict)


@dataclass
class Sensor:
    # pylint:disable=too-many-instance-attributes
    """Class representing a single sensor"""

    type: str
    theta_max: float
    phi_max: float
    sens_range: float
    sensor_region: SensorRegion
    location: np.array
    orientation: np.array
    active = bool = False
    price: float = 1.0

    def is_valid(self) -> bool:
        """Check to ensure the sensor is located on the sensor region"""
        return np.dot(self.sensor_region.zhat, self.location) < 1e-14

    def can_see(self, point: RoiPoint, delta: np.ndarray = None, verbose: bool = False) -> float:
        # pylint:disable=too-many-locals
        """Determine if the sensor covers ('sees') the passed point

        Args:
            point: test point that the sensor is trying to 'see'
            delta: proposed changes to sensor location and orientation
            verbose: output additional information (mostly for debug)

        Returns:
            float: 0 if sensor can't see point, point's region 'value' if it can
        """
        val = point.critical_index
        if delta is not None:
            xhat = self.sensor_region.xhat
            yhat = self.sensor_region.yhat
            loc = self.location + delta[0] * xhat + delta[1] * yhat
            theta_o = self.orientation[0] + delta[2]
            phi_o = self.orientation[1] + delta[3]
        else:
            loc = self.location
            theta_o = self.orientation[0]
            phi_o = self.orientation[1]

        r = self.sensor_region.get_length(point.pos, loc)
        theta = self.sensor_region.get_theta(point.pos, loc)
        phi = self.sensor_region.get_phi(point.pos, loc)
        in_range = r <= self.sens_range
        in_theta_angle = self.theta_max >= 2 * abs(theta_o - theta)
        in_phi_angle = self.phi_max >= 2 * abs(phi_o - phi)
        if verbose:
            print(f"In Range:{in_range}, Theta: {in_theta_angle}, " f"Phi: {in_phi_angle}")
            print(
                f"Theta:{theta},Theta_o{theta_o},Theta_max:{self.theta_max}\n "
                f"Phi:{phi},Phi_o:{phi_o},Phi_max:{self.phi_max}\n"
                f"r:{r},Range:{self.sens_range}"
            )
            local_pos = self.sensor_region.get_local_pos(point.pos, loc)
            print(f"Local coords:{local_pos}")

        return int(in_range and in_theta_angle and in_phi_angle) * val

    def to_dict(self) -> dict:
        # pylint:disable=attribute-defined-outside-init
        """Returns sensor object as a dictionary"""
        sensor = copy(self)
        sensor.loc_x = self.location[0]
        sensor.loc_y = self.location[1]
        sensor.loc_z = self.location[2]
        sensor.region = self.sensor_region.region
        return sensor.__dict__

    def to_df(self) -> pd.DataFrame:
        """Returns sensor object as a dataframe"""
        sensor_dict = self.to_dict()
        sensor_df = pd.DataFrame.from_dict(sensor_dict, orient="index").transpose()
        sensor_df.drop(columns="location", inplace=True)
        sensor_df.drop(columns="sensor_region", inplace=True)
        return sensor_df


class RegionOfInterest(ABC):
    """Abstract base class for region of interest objects"""

    @abstractmethod
    def sample_points(self, num_points: int) -> list[RoiPoint]:
        """Randomly selects points from inside this region of interest

        Args:
            num_points: number of points to select

        Returns:
            list[RoiPoint]
        """

    @property
    @abstractmethod
    def volume(self) -> float:
        """Calculates the volume of this region"""

    @property
    @abstractmethod
    def weighted_volume(self) -> float:
        """Calculates the weighted volume of the region"""

    @classmethod
    @abstractmethod
    def from_dict(cls, spec_dict: dict) -> RegionOfInterest:
        """Creates ROI object from passed dictionary"""


@dataclass
class CubicROI(RegionOfInterest):
    """Class representing a cubic shaped Region of Interest"""

    x_range: np.array
    y_range: np.array
    critical_index: float
    z_range: np.array = np.array([0, 0])

    def sample_points(self, num_points: int) -> list[RoiPoint]:
        """Randomly selects points from inside this region of interest

        Args:
            num_points: number of points to select

        Returns:
            list[RoiPoint]
        """
        xs = np.random.uniform(low=self.x_range[0], high=self.x_range[1], size=num_points)
        ys = np.random.uniform(low=self.y_range[0], high=self.y_range[1], size=num_points)
        if np.diff(self.z_range) != 0:
            zs = np.random.uniform(low=self.z_range[0], high=self.z_range[1], size=num_points)
        else:
            zs = np.zeros(num_points)

        return [RoiPoint(xs[i], ys[i], zs[i], self.critical_index) for i in range(num_points)]

    @property
    def volume(self) -> float:
        """Calculates the volume of this region"""
        area = np.diff(self.x_range) * np.diff(self.y_range)
        if np.diff(self.z_range) != 0:
            return area[0] * np.diff(self.z_range)[0]
        return area[0]

    @property
    def weighted_volume(self) -> float:
        """Calculates the weighted volume of the region"""
        return self.critical_index * self.volume

    @classmethod
    def from_dict(cls, spec_dict: dict) -> CubicROI:
        """Creates CubicROI object from passed dictionary

         Args:
             spec_dict: dictionary of region specifications

        Returns:
             CubicROI
        """
        x_range = np.array([spec_dict["start_point_x"], spec_dict["end_point_x"]])
        y_range = np.array([spec_dict["start_point_y"], spec_dict["end_point_y"]])
        z_range = np.array([spec_dict["start_point_z"], spec_dict["end_point_z"]])
        critical_index = spec_dict["critical_index"]
        return cls(
            x_range=x_range,
            y_range=y_range,
            z_range=z_range,
            critical_index=critical_index,
        )


@dataclass
class SectorROI(RegionOfInterest):
    """Class representing a sector shaped Region of Interest"""

    centre: np.array
    min_radius: float
    max_radius: float
    centre_angle: float
    angle_of_view: float
    critical_index: float
    height: float = 0.0

    def sample_points(self, num_points: int) -> list[RoiPoint]:
        """Randomly selects points from inside this region oof interest

        Args:
            num_points: number of points to select

        Returns:
            list[RoiPoint]
        """
        x_centre, y_centre, z_centre = self.centre
        radius = np.random.uniform(low=self.min_radius, high=self.max_radius, size=num_points)
        angle = np.random.uniform(
            low=self.centre_angle - self.angle_of_view / 2,
            high=self.centre_angle + self.angle_of_view / 2,
            size=num_points,
        )

        xs, ys = self.polar_to_cart(radius, angle)
        xs = x_centre + xs
        ys = y_centre + ys

        if self.height != 0:
            zs = np.random.uniform(low=z_centre, high=z_centre + self.height, size=num_points)
        else:
            zs = z_centre * np.ones(num_points)

        return [RoiPoint(xs[i], ys[i], zs[i], self.critical_index) for i in range(num_points)]

    @staticmethod
    def polar_to_cart(radius: float, angle: float) -> tuple(float, float):
        """Converts polar coords to cartesian coords"""
        x = radius * np.cos(angle)
        y = radius * np.sin(angle)
        return x, y

    @property
    def volume(self) -> float:
        """Calculates the volume of this region"""
        area = np.pi * self.angle_of_view * (self.max_radius**2 - self.min_radius**2)
        if self.height != 0:
            return area * self.height
        return area

    @property
    def weighted_volume(self) -> float:
        """Calculates the weighted volume of the region"""
        return self.critical_index * self.volume

    @classmethod
    def from_dict(cls, spec_dict: dict, in_degrees: bool = False) -> SectorROI:
        """Creates SectorROI object from passed dictionary

         Args:
             spec_dict: dictionary of region specifications
             in_degrees: specifies if region angles are in degrees

        Returns:
             SectorROI
        """
        centre = np.array(
            [
                spec_dict["centre_x"],
                spec_dict["centre_y"],
                spec_dict["centre_z"],
            ]
        )
        min_radius = spec_dict["radius_min"]
        max_radius = spec_dict["radius_max"]
        centre_angle = spec_dict["yaw_angle"]
        aov = spec_dict["h_angle"]
        if in_degrees:
            centre_angle = centre_angle / 180 * np.pi
            aov = aov / 180 * np.pi
        critical_index = spec_dict["critical_index"]
        height = spec_dict["height"]
        return cls(
            centre=centre,
            min_radius=min_radius,
            max_radius=max_radius,
            centre_angle=centre_angle,
            angle_of_view=aov,
            height=height,
            critical_index=critical_index,
        )


class SensorConfiguration:
    """Class to track the configuration, and coverage of the sensors

    Attributes:
        sensor_list (list[Sensor]): list of sensors to optimize
        sensor_df (pd.DataFrame): dataframe of current sensor configuration
    """

    def __init__(self, sensor_list: list[Sensor]) -> None:
        """
        Args:
            sensor_list: list of sensors to optimize
        """
        self.sensor_list = sensor_list
        self.sensor_df = self._make_sensor_df(sensor_list)

    def coverage(
        self,
        points: list[RoiPoint],
        only_active: bool = True,
        delta_loc_orient: np.ndarray = None,
    ) -> tuple(np.array, float):
        """Calcs weighted volume of test pnts 'seen' by current sensor cfg

        This method can calculate either the 'static' coverage from current
        sensor configuration, or (if delta_loc_orient is passed) it can also
        calculate the 'new' coverage if the current sensor configuration was
        changed by the values in delta_loc_orient.

        Args:
            points: test points to be used when calculating coverage
            only_active: calculate coverage only using active sensors
            delta_loc_orient: proposed changes in sensor locations and
            orientations
        """
        coverages = []
        valid_sensor_df = self.sensor_df[self.sensor_df.valid is True]
        if only_active:
            valid_sensor_df = valid_sensor_df[self.sensor_df.active is True]
        sensor_indices = valid_sensor_df.index.values
        sensors = [self.sensor_list[index] for index in sensor_indices]
        for i, sensor in enumerate(sensors):
            if delta_loc_orient is None:
                coverages.append([sensor.can_see(point) for point in points])
            else:
                delta = delta_loc_orient[4 * i : 4 * (i + 1)]
                coverages.append([sensor.can_see(point, delta) for point in points])
        coverages = np.array(coverages)
        fraction = coverages.sum() / sum([pt.critical_index for pt in points])
        return coverages, fraction

    def _make_sensor_df(self, sensor_list: list[Sensor]) -> pd.DataFrame:
        sensor_df = pd.concat([sens.to_df() for sens in sensor_list], axis=0)
        sensor_df.reset_index(inplace=True, drop=True)
        sensor_df["active"] = ["False"] * sensor_df.shape[0]
        sensor_df["valid"] = [sensor.is_valid() for sensor in sensor_list]
        return sensor_df

    @classmethod
    def from_dfs(
        cls,
        initial_df: pd.DataFrame,
        sensor_types_df: pd.DataFrame,
        allowed_regions_df: pd.DataFrame,
    ) -> SensorConfiguration:
        """Creates SensorConfiguration object from passed dfs

        Args:
            initial_df: dataframe of initial sensor setup
            sensor_types_df: dataframe of sensor type data
            allowed_regions_df: dataframe of allowed sensor_regions

        Returns:
            SensorConfiguration
        """
        sensors = []
        for _, row in initial_df.iterrows():
            sens_type = row["type"]
            loc = np.array([row["loc_x"], row["loc_y"], row["loc_z"]])
            region = next(r for r in allowed_regions_df if r.region == row["region"])
            orientation = np.array([row["theta"], row["phi"]])
            sensor = Sensor(
                **sensor_types_df.loc[sens_type].to_dict(),
                location=loc,
                sensor_region=region,
                type=sens_type,
                orientation=orientation,
            )
            sensors.append(sensor)
        return cls(sensors)


class SensorConfigOptimizer:
    """Class to manage optimization of the sensor configuration

    Attributes:
        sampler (PointSampler): point sampler object to generate test points
        sensor_config (SensorCon): object that holds 'current' sensor
        configuration
    """

    def __init__(self, sampler: PointSampler, sensor_config: SensorConfiguration) -> None:
        """
        Args:
            sampler: point sampler object to generate test points
            sensor_config: object that holds 'current' sensor configuration
        """
        self.sampler = sampler
        self.sensor_config = sensor_config

    def optimize_sensor_configuration(
        self,
        max_iterations: int = 100,
        num_test_points: int = 1000,
        type_method: str = "tabu",
        loc_orient_method: str = "Nelder-Mead",
    ) -> None:
        """Optimize the location, orientation, and type of sensors

        Note: method does not return anything, but iteratively updates the
        sensor_config object to reflect optimized sensor location, orientation,
        type.

        Args:
            max_iterations: maximum number of optimization iterations to attempt
            num_test_points: number of tests points to assess sensor coverage
            type_method: algorithm to optimize sensor types
            loc_orient_method: algorithm to optimize location and orientation
        """
        # TODO: Also allow for stopping criteria (convergence of objective)
        for _ in tqdm.tqdm(range(max_iterations)):
            test_points = self.sampler.generate_points(num_test_points)

            # optimize sensor configuration
            self.optimize_sens_loc_orient_change(points=test_points, method=loc_orient_method)

            # optimize sensor types
            self.optimize_sens_types(points=test_points, method=type_method)

    def optimize_sens_types(self, points: list[RoiPoint], method: str = "tabu") -> None:
        """Optimize sensor types

        Assumes sensor locations and orientations are fixed.
        Note: method does not return anything, but updates the sensor_df
        attribute of the sensor_config object to reflect optimized sensor types
        Args:
            points: Test points to evaluate sensor coverage at
            method: optimization algorithm
        """
        coverage, _ = self.sensor_config.coverage(points, only_active=False)
        qubo = self._get_qubo(coverage)
        if method == "exact":
            dict_bqm = dimod.BQM.from_qubo(qubo)
            sampler_exact = dimod.ExactSolver()
            response = sampler_exact.sample(dict_bqm)
            print(response)
        elif method == "tabu":
            response = TabuSampler().sample_qubo(qubo)
            print(response)
        result = response.record[0][0]
        self.sensor_config.sensor_df["active"] = result

    def optimize_sens_loc_orient_change(
        self, points: list[RoiPoint], method: str = "Nelder-Mead"
    ) -> None:
        """Find optimal change in location and orientation of sensors

        Assumes sensor types are fixed. Method does not return anything, but updates the sensor_df
        attribute of the sensor_config object to reflect optimized sensor types
        Args:
            points: Test points to evaluate sensor coverage at
            method: optimization algorithm
        """
        num_active = self.sensor_config.sensor_df["active"].sum()
        wvf = self._make_wvf(points)
        # TODO: Use COBYLA with LinearConstraint
        optimal_change = minimize(wvf, np.zeros(4 * num_active), method=method)
        sensor_df = self.sensor_config.sensor_df
        sensor_df = sensor_df[sensor_df.active == 1]
        sensor_indices = sensor_df.index.values
        sensors = [self.sensor_config.sensor_list[index] for index in sensor_indices]
        for i, sensor in enumerate(sensors):
            delta = optimal_change.x[4 * i : 4 * (i + 1)]
            xhat = sensor.sensor_region.xhat
            yhat = sensor.sensor_region.yhat
            sensor.location = sensor.location + delta[0] * xhat + delta[1] * yhat
            sensor.orientation = sensor.orientation[0] + delta[2]
            sensor.orientation = sensor.orientation[1] + delta[3]

    def _make_wvf(self, points: list[RoiPoint]) -> Callable:
        """Given fixed test points, create weighted volume function for use in
        optimizer"""

        def wvf(delta_loc_orient):
            """Weighted volume function for a fixed set of test points

            Args:
                delta_loc_orient: change in location and orientation of
                sensors. This is optimized to find optimal change that
                will maximize weighted volume
            """
            _, frac = self.sensor_config.coverage(points, delta_loc_orient=delta_loc_orient)
            return -frac

        return wvf

    def _get_qubo(self, coverages: np.array) -> dict:
        # pylint:disable=too-many-locals
        """Create quadratic unconstrainied binary objective of sensor types

        Coverages is a 2d boolean array with rows as sensors, columns as
        test_points. An entry of 1 for row i and column j  indicates that
        test_point j is 'seen' by sensor i. This method creates a qubo to
        change types of sensors such that as many columns are covered with as
        few duplicate coverages (points covered by more than one sensor) as
        possible. So when points are covered, that is rewarded (with a negative
        value) whereas when points are covered by the ith and jth qubit, that
        is penalized.
        """
        # Points that have non-zero coverage
        nzs = np.unique(coverages.nonzero()[1])  # index of covered points
        is_covered = np.array(coverages != 0).astype(int)

        # only points that have non-zero coverage
        nz_cov = coverages[:, nzs]
        covered_point_reward = -nz_cov.sum(axis=1)

        # points that are covered exactly twice
        pairs = np.where(is_covered.sum(axis=0) == 2)[0]
        cov_pairs = coverages[:, pairs]
        num_cols = cov_pairs.shape[1]

        # create pairwise qubit penalties (two sensors cover same point)
        quadratic_penalties = {}
        for j in range(num_cols):
            pair = np.where(cov_pairs[:, j] != 0)[0]
            pair.sort()
            try:
                quadratic_penalties[tuple(pair)] += cov_pairs[0, j]
            except KeyError:
                quadratic_penalties.update({tuple(pair): cov_pairs[0, j]})
        max_reward = max(abs(covered_point_reward))
        try:
            max_quad_penalty = max([abs(val) for val in quadratic_penalties.values()])
        except ValueError:
            max_quad_penalty = 0
        max_coeff = max(max_reward, max_quad_penalty)

        # normalize qubo values
        if max_coeff != 0:
            covered_point_reward /= max_coeff
            for key in quadratic_penalties:
                quadratic_penalties[key] /= max_coeff
        qubo = dict(quadratic_penalties)

        # Overwrite the ith - ith quadratic penalty with ith linear reward
        for i, penalty in enumerate(covered_point_reward):
            qubo.update({(i, i): penalty})

        sensors = self.sensor_config.sensor_list
        for i, sensor1 in enumerate(sensors):
            for j, sensor2 in enumerate(sensors):
                if j > i:
                    if all(sensor1.location == sensor2.location):
                        qubo[(i, j)] = SAME_LOCATION_PENALTY
        return qubo


class ROIsFactory:
    """Handles construction of regions of interest from file

    Attributes:
        sector_roi_df (pd.DataFrame): dataframe specifying sector shaped
        regions of interest
        cubic_roi_df (pd.DataFrame): dataframe specifying cubic shaped
        regions of interest
    """

    def __init__(self, sector_roi_path: str, cubic_roi_path: str) -> None:
        """
        Args:
            sector_roi_path: path containing sector shaped regions of interest
            cubic_roi_path: path containing cubic shaped regions of interest
        """
        self.sector_roi_df = pd.read_csv(sector_roi_path, delimiter=";")
        self.cubic_roi_df = pd.read_csv(cubic_roi_path, delimiter=";")

    def make_rois(self) -> list[RegionOfInterest]:
        """Create list of regions of interest

        Returns:
            list[RegionOfInterest]
        """
        # TODO: this always uses degrees. Need to fix
        sector_roi_dict = self.sector_roi_df.to_dict(orient="index")
        cubic_roi_dict = self.cubic_roi_df.to_dict(orient="index")
        sector_rois = [
            SectorROI.from_dict(row_dict, in_degrees=True)
            for _, row_dict in sector_roi_dict.items()
        ]
        cubic_rois = [CubicROI.from_dict(row_dict) for _, row_dict in cubic_roi_dict.items()]
        return sector_rois + cubic_rois


class SensorConfigurationFactory:
    """Handles construction of SensorConfiguration object from files

    Attributes:
        sensor_types_df (pd.DataFrame): dataframe containing sensor types
        sensor_regions_df (pd.DataFrame): dataframe containing sensor rgns
        initial_config_df (pd.DataFrame): dataframe of initial sensor
        configurations
    """

    def __init__(
        self, sensor_type_path: str, sensor_regions_path: str, initial_config_path
    ) -> None:
        """
        Args:
            sensor_type_path: path to sensor types data
            sensor_regions_path: path to allowable sensor regions data
            initial_config_path: path to initial config data
        """
        self.sensor_types_df = self._parse_sensor_type_file(sensor_type_path)
        self.sensor_regions_df = self._parse_sensor_regions_file(sensor_regions_path)
        self.initial_config_df = self._parse_initial_config_file(initial_config_path)

    def make_sensor_configuration(self) -> SensorConfiguration:
        """Creates SensorConfiguration object

        Returns:
            SensorConfiguration
        """
        return SensorConfiguration.from_dfs(
            self.initial_config_df, self.sensor_types_df, self.sensor_regions_df
        )

    def _parse_sensor_type_file(self, sensor_type_path: str) -> pd.DataFrame:
        """Read in sensor type data and create related df"""
        sensor_types_df = pd.read_csv(sensor_type_path, index_col="type")
        sensor_types_df["theta_max"] = sensor_types_df["angle_h"] / 180 * np.pi
        sensor_types_df["phi_max"] = sensor_types_df["angle_v"] / 180 * np.pi
        sensor_types_df.drop(columns=["angle_h", "angle_v"], inplace=True)
        return sensor_types_df

    def _parse_sensor_regions_file(self, sensor_regions_path: str) -> list[SensorRegion]:
        """Read in sensor region data and create related df"""
        regions = []
        sensor_regions_df = pd.read_csv(sensor_regions_path, delimiter=";")
        all_regions = sensor_regions_df.iloc[:, :14].to_dict(orient="index")
        for _, region_dict in all_regions.items():
            regions.append(SensorRegion.from_dict(region_dict))
        return regions

    def _parse_initial_config_file(self, initial_config_path: str) -> pd.DataFrame:
        """Create initial sensor config data and create related dataframe"""
        initial_sensor_df = pd.read_csv(initial_config_path, index_col="var")
        initial_sensor_df["theta"] = initial_sensor_df["theta"] / 180 * np.pi
        initial_sensor_df["phi"] = initial_sensor_df["phi"] / 180 * np.pi
        return initial_sensor_df


@click.command()
@click.argument(
    "sector_roi_file",
    nargs=1,
    type=click.Path(exists=True),
    default="../data/roi_sector.csv",
)
@click.argument(
    "cubic_roi_file",
    nargs=1,
    type=click.Path(exists=True),
    default="../data/roi_cubic.csv",
)
@click.argument(
    "sensor_type_file",
    nargs=1,
    type=click.Path(exists=True),
    default="../data/sensortypes.csv",
)
@click.argument(
    "allowed_sensor_positions",
    nargs=1,
    type=click.Path(exists=True),
    default="../data/allowed_sensor_positions.csv",
)
@click.argument(
    "initial_config",
    nargs=1,
    type=click.Path(exists=True),
    default="../data/sensorconfig.csv",
)
@click.option(
    "--max_iterations",
    "-m",
    type=click.INT,
    default=100,
    help="Maximum number of iterations to be carried out during " "optimization",
)
@click.option(
    "--num_test_points",
    "-n",
    type=click.INT,
    default=1000,
    help="Number of test points to use when calculating coverage",
)
@click.option(
    "--type_method",
    "-t",
    type=click.Choice(["tabu", "exact"], case_sensitive=False),
    help="Solver to use for optimizing sensor types",
    default="tabu",
)
@click.option(
    "--loc_orient_method",
    "-l",
    type=click.Choice(["Nelder-Mead"], case_sensitive=False),
    help="Solver to use for optimizing sensor location and " "orientation",
    default="Nelder-Mead",
)
@click.option("--output", "-o", type=str, help="Path to output file", default="out.csv")
def main(
    sector_roi_file,
    cubic_roi_file,
    sensor_type_file,
    allowed_sensor_positions,
    initial_config,
    max_iterations,
    num_test_points,
    type_method,
    loc_orient_method,
    output,
):
    # pylint:disable=too-many-arguments
    # pylint:disable=too-many-locals

    """Main function to run the sensor optimization"""

    # Factories to create required objects from file
    rois_fac = ROIsFactory(sector_roi_file, cubic_roi_file)
    sensor_cfg_fac = SensorConfigurationFactory(
        sensor_type_file, allowed_sensor_positions, initial_config
    )

    # Create regions of interest, point sampler, and initial configuration
    rois = rois_fac.make_rois()
    sampler = PointSampler(rois)
    sensor_config = sensor_cfg_fac.make_sensor_configuration()

    # Create configuration optimizer object
    optimizer = SensorConfigOptimizer(sampler, sensor_config)

    # Optimize
    optimizer.optimize_sensor_configuration(
        max_iterations, num_test_points, type_method, loc_orient_method
    )

    # Output result (dataframe of sensor locations, orientations, and types)
    optimizer.sensor_config.sensor_df.to_csv(output)


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    main()
